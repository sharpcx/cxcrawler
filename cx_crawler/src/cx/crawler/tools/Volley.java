/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cx.crawler.tools;

import cx.crawler.base.BasicNetwork;
import cx.crawler.base.Cache;
import cx.crawler.base.HttpStack;
import cx.crawler.base.Network;
import cx.crawler.base.RequestQueue;

import java.io.File;

import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;

public class Volley {

	/** Default on-disk cache directory. */
	private static final String DEFAULT_CACHE_DIR = "volley";

	/**
	 * Creates a default instance of the worker pool and calls
	 * {@link RequestQueue#start()} on it.
	 * 
	 * @param context
	 *            A {@link Context} to use for creating the cache dir.
	 * @param stack
	 *            An {@link HttpStack} to use for the network, or null for
	 *            default.
	 * @return A started {@link RequestQueue} instance.
	 */
	public static RequestQueue newRequestQueue(HttpStack stack) {

		String userAgent = "volley/0";

		if (stack == null) {
			HttpParams params = new BasicHttpParams();
			
			params.setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.BROWSER_COMPATIBILITY);
			params.setParameter(CoreProtocolPNames.USER_AGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0) ");
			params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 1000);
			params.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 1000);

			params.setBooleanParameter("http.protocol.handle-redirects", false);

			SchemeRegistry schemeRegistry = new SchemeRegistry();
			schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));

			PoolingClientConnectionManager connectionManager = new PoolingClientConnectionManager(schemeRegistry);
			connectionManager.setMaxTotal(10);
			connectionManager.setDefaultMaxPerRoute(10);
			DefaultHttpClient httpClient = new DefaultHttpClient(connectionManager, params);

			stack = new HttpClientStack(httpClient);
		}

		Network network = new BasicNetwork(stack);
		Cache noCache=new NoCache();
		RequestQueue queue = new RequestQueue(noCache, network);
		queue.start();

		return queue;
	}

	/**
	 * Creates a default instance of the worker pool and calls
	 * {@link RequestQueue#start()} on it.
	 * 
	 * @param context
	 *            A {@link Context} to use for creating the cache dir.
	 * @return A started {@link RequestQueue} instance.
	 */
	public static RequestQueue newRequestQueue() {
		return newRequestQueue(null);
	}
}
