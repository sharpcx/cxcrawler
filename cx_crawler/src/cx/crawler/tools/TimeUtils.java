package cx.crawler.tools;

import java.util.Date;

public class TimeUtils {
	public static long getCurrentTimeMillinseconds(){
		return (new Date()).getTime();
	}
}
