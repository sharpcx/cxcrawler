package cx.crawler.base;

public class Context {
	private RequestQueue requestQueue;
	private NetworkResponse response;
	private Request request;
	
	
	public RequestQueue getRequestQueue() {
		return requestQueue;
	}
	public void setRequestQueue(RequestQueue requestQueue) {
		this.requestQueue = requestQueue;
	}
	public NetworkResponse getResponse() {
		return response;
	}
	public void setResponse(NetworkResponse response) {
		this.response = response;
	}
	public Request getRequest() {
		return request;
	}
	public void setRequest(Request request) {
		this.request = request;
	}
}
