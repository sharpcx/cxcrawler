/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cx.crawler.base;


/**
 * 判断用户是否有请求权限
 */
@SuppressWarnings("serial")
public class AuthFailureError extends VolleyError {
	
    public AuthFailureError() { }

//    public AuthFailureError(String intent) {
////        mResolutionIntent = intent;
//    }

    public AuthFailureError(NetworkResponse response) {
//        super(response);
    }

    public AuthFailureError(String message) {
//        super(message);
    }

    public AuthFailureError(String message, Exception reason) {
//        super(message, reason);
    }

//    public String getResolutionIntent() {
////        return mResolutionIntent;
//    }

    @Override
    public String getMessage() {
//        if (mResolutionIntent != null) {
//            return "User needs to (re)enter credentials.";
//        }
        return "该用户没有请求权限！！"+super.getMessage();
    }
}
