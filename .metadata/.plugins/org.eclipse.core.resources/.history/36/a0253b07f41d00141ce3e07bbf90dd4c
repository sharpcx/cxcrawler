/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package cx.crawler.base;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;

import edu.uci.ics.crawler4j.parser.TextParseData;

/**
 * Data and headers returned from {@link Network#performRequest(Request)}.
 */
public class NetworkResponse {
    /**
     * Creates a new network response.
     * @param statusCode the HTTP status code
     * @param data Response body
     * @param headers Headers returned with this response, or null for none
     * @param notModified True if the server returned a 304 and the data was already in cache
     */
    public NetworkResponse(int statusCode, byte[] data, Map<String, String> headers,
            boolean notModified,HttpEntity httpEntity) {
        this.statusCode = statusCode;
        this.data = data;
        this.headers = headers;
        this.notModified = notModified;
        this.httpEntity=httpEntity;
        init();
    }

    public NetworkResponse(byte[] data) {
        this(HttpStatus.SC_OK, data, Collections.<String, String>emptyMap(), false,null);
    }

    public NetworkResponse(byte[] data, Map<String, String> headers) {
        this(HttpStatus.SC_OK, data, headers, false,null);
    }

    /** The HTTP status code. */
    public final int statusCode;

    /** Raw data from this response. */
    public final byte[] data;

    /** Response headers. */
    public final Map<String, String> headers;

    /** True if the server returned a 304 (Not Modified). */
    public final boolean notModified;
    
    /**HttpEntity**/
    public final HttpEntity httpEntity;
    
    /**ContentType**/
    public String contentType; 
    
    /**ContentEncoding**/
    public Charset charset;
    public String contentCharset;
    public String contentEncoding;
    
    /**ContentData**/
//    public byte[] contentData;
    
    private void init(){
    	contentType = null;
		Header type = httpEntity.getContentType();
		if (type != null) {
			contentType = type.getValue();
		}

		contentEncoding = null;
		Header encoding = httpEntity.getContentEncoding();
		if (encoding != null) {
			contentEncoding = encoding.getValue();
		}

		Charset charset = ContentType.getOrDefault(httpEntity).getCharset();
		if (charset != null) {
			contentCharset = charset.displayName();	
		}

		try {
			contentData = EntityUtils.toByteArray(httpEntity);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public String getHtmlText(){
    	if(contentType.contains("text/plain")){
    		try {
    			String resultString="";
				if (contentCharset == null) {
					resultString=new String(contentData);
				} else {
					resultString=new String(page.getContentData(), page.getContentCharset());
				}
				return resultString;
			} catch (Exception e) {
				logger.error(e.getMessage() + ", while parsing: " + page.getWebURL().getURL());
			}
    		
    		
    	}
    }
    
    
    
    
    
    
    
    
    
}